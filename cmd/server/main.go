package main

import (
	"fmt"
	"net/http"

	"gitlab.com/hex-addicts/goulette/bots/chitchat"
	"gitlab.com/hex-addicts/goulette/bots/echo"
	"gitlab.com/hex-addicts/goulette/server"
	"gitlab.com/hex-addicts/goulette/server/transport/jsonws"
	"golang.org/x/net/websocket"
)

func registerConnection(server *server.Server) func(ws *websocket.Conn) {
	return func(ws *websocket.Conn) {
		fmt.Println("New connection from", ws.Request().RemoteAddr)

		doneChan := server.RegisterConnection(jsonws.NewTransport(ws))
		<-doneChan // Block until the connection is closed
	}
}

func main() {

	fmt.Println("Goulette chat server v0.1")

	server := server.New(server.WithDefaultHandlers)

	server.RegisterBotConnection(chitchat.NewBot("marujaXXX"))
	server.RegisterBotConnection(echo.NewBot("manolo69"))

	http.Handle("/ws", websocket.Handler(registerConnection(server)))

	http.ListenAndServe(":8080", nil)
}
