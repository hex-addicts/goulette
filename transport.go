package goulette

// Transport abstracts the details of the protocol used for connections
type Transport interface {
	Send(key string, data interface{}) error
	Receive() (key string, data []byte, err error)
	DecodeInto(data []byte, dest interface{}) error
	Close() error
}
