package echo

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/hex-addicts/goulette"
	"gitlab.com/hex-addicts/goulette/bots/internal/base"
)

type speakData struct {
	Message string `json:"message"`
}

// WithDebug enables debug mode for the bot's connection
func WithDebug(bot *Bot) {
	bot.debug = true
}

// Bot just responds messages with "you said: xxxx"
type Bot struct {
	*base.Bot
	isLogged       bool
	isDebugSent    bool
	debug          bool
	isGreetingSent bool
	messages       chan string
}

// NewBot allocates and returns a new Echo Bot
func NewBot(nick string, opts ...func(*Bot)) *Bot {
	bot := &Bot{
		Bot:      base.NewBot(nick),
		messages: make(chan string, 100),
	}

	for _, optFunc := range opts {
		optFunc(bot)
	}

	return bot
}

// Send implementation of the chat.Transport interface
func (bot *Bot) Send(event string, data interface{}) error {

	if event != goulette.EventChatMessageReceived {
		return nil
	}

	msgData := speakData{}
	err := bot.DecodeInto(bot.MustEncodeData(data), &msgData)
	if err != nil {
		return err
	}

	bot.messages <- msgData.Message

	return nil
}

// Receive implementation of the chat.Transport interface
func (bot *Bot) Receive() (string, []byte, error) {
	if bot.debug && !bot.isDebugSent {
		bot.isDebugSent = true
		return bot.CommandDebugOn()
	}

	if !bot.isLogged {
		bot.isLogged = true
		return bot.CommandLogin()
	}

	msg := <-bot.messages
	ms := rand.Intn(5000)
	time.Sleep(time.Duration(ms) * time.Millisecond)
	return bot.CommandSpeak(fmt.Sprintf("you said: %s", msg))
}
