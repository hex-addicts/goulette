package base

import (
	"encoding/json"

	"gitlab.com/hex-addicts/goulette"
)

type speakData struct {
	Message string `json:"message"`
}

type loginData struct {
	Nick string `json:"nick"`
}

func NewBot(nick string) *Bot {
	return &Bot{
		nick: nick,
	}
}

func (bot *Bot) Nick() string {
	return bot.nick
}

// Bot just responds messages with "you said: xxxx"
type Bot struct {
	nick string
}

// DecodeInto implementation of the chat.Transport interface
func (bot *Bot) DecodeInto(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

// Close implementation of the chat.Transport interface
func (bot *Bot) Close() error {
	return nil
}

func (bot *Bot) CommandSpeak(message string) (string, []byte, error) {
	msg := speakData{
		Message: message,
	}
	data, err := json.Marshal(msg)

	return goulette.CommandChatSpeak, data, err
}

func (bot *Bot) CommandLogin() (string, []byte, error) {
	msg := loginData{
		Nick: bot.nick,
	}
	data, err := json.Marshal(msg)

	return goulette.CommandConnectionLogin, data, err
}

func (bot *Bot) CommandDebugOn() (string, []byte, error) {
	return goulette.CommandConnectionDebugEnable, nil, nil
}

func (bot *Bot) MustEncodeData(data interface{}) []byte {
	encoded, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}

	return encoded
}
