package chitchat

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/hex-addicts/goulette/bots/internal/base"
)

// WithDebug enables the debug mode for the bot
func WithDebug(bot *Bot) {
	bot.debug = true
}

// WithLines sets the conversation lines to be used
func WithLines(lines ...string) func(bot *Bot) {
	return func(bot *Bot) {
		bot.lines = lines
	}
}

// Bot just says random phrases from a list
type Bot struct {
	*base.Bot
	debug          bool
	isLogged       bool
	isDebugSent    bool
	isGreetingSent bool
	lines          []string
}

// NewBot allocates and returns a new Echo Bot
func NewBot(nick string, opts ...func(*Bot)) *Bot {
	bot := &Bot{
		Bot:   base.NewBot(nick),
		lines: lines,
	}

	for _, optFunc := range opts {
		optFunc(bot)
	}

	return bot
}

// Send implementation of the chat.Transport interface
func (bot *Bot) Send(command string, data interface{}) error {
	return nil
}

// Receive implementation of the chat.Transport interface
func (bot *Bot) Receive() (string, []byte, error) {
	if bot.debug && !bot.isDebugSent {
		bot.isDebugSent = true
		return bot.CommandDebugOn()
	}

	if !bot.isLogged {
		bot.isLogged = true
		return bot.CommandLogin()
	}

	// delay between 5 and 15 seconds
	time.Sleep(time.Duration(5000+rand.Intn(10000)) * time.Millisecond)

	return bot.saySomething()
}

func (bot *Bot) saySomething() (string, []byte, error) {
	if !bot.isGreetingSent {
		bot.isGreetingSent = true
		return bot.CommandSpeak(fmt.Sprintf("Oh hey!  %s here!", bot.Nick()))
	}

	return bot.CommandSpeak(randomLine())
}

func randomLine() string {
	i := rand.Intn(len(lines))
	return lines[i]
}

var lines = []string{
	"Halo??",
	"Nice day, huh?",
	"I guess you're just quiet...",
	"Tell me something abut you",
	"Business, or pleasure?",
	"Are you a cat person, or a dog person?",
}
