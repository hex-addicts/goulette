FROM golang:1.11

ENV PACKAGE_PATH /go/src/gitlab.com/hex-addicts/goulette

RUN go get \
    github.com/cespare/reflex 

RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

RUN mkdir -p $PACKAGE_PATH
COPY . $PACKAGE_PATH

RUN ln -s $PACKAGE_PATH /code

VOLUME ["/code"]

WORKDIR /code

CMD reflex -r '\.go$' -s -- sh -c 'cd cmd/server && go build -o goulette-server && ./goulette-server'
