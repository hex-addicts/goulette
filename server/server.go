package server

import (
	"errors"
	"fmt"
	"sync"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/hex-addicts/goulette"
)

const (
	maxRecentNicks = 3
)

// Connection sends events to and receives events from the server
type Connection struct {
	lock sync.RWMutex
	goulette.Transport
	debug       bool // if true, the server sends the received commands back
	nick        string
	isBot       bool
	server      *Server
	lastActive  time.Time
	currentChat *chat
	recentNicks nickList
}

// Close closes the connection, ending any running chats
func (conn *Connection) Close() error {
	return conn.Transport.Close()
}

// Server handles chats and connections
type Server struct {
	lock        sync.RWMutex
	conns       map[*Connection]bool
	nicks       map[string]*Connection
	botConns    map[*Connection]bool
	chats       map[*chat]bool
	waitingConn *Connection
	handlers    map[string][]CommandHandler
}

// New allocates and returns a new chat server
func New(opts ...func(*Server)) *Server {
	s := &Server{
		chats:    make(map[*chat]bool),
		conns:    make(map[*Connection]bool),
		botConns: make(map[*Connection]bool),
		handlers: make(map[string][]CommandHandler),
	}

	for _, optFunc := range opts {
		optFunc(s)
	}

	return s
}

// RegisterConnection registers a connection to the server chat
func (s *Server) RegisterConnection(tr goulette.Transport) <-chan bool {
	return s.registerConnection(tr, false)
}

// RegisterBotConnection registers a connection as bot
func (s *Server) RegisterBotConnection(tr goulette.Transport) <-chan bool {
	return s.registerConnection(tr, true)
}

func (s *Server) registerConnection(tr goulette.Transport, isBot bool) <-chan bool {
	conn := &Connection{
		Transport:   tr,
		server:      s,
		isBot:       isBot,
		lastActive:  time.Now(),
		recentNicks: nickList{maxItems: maxRecentNicks},
	}

	s.lock.Lock()
	s.conns[conn] = true
	if isBot {
		s.botConns[conn] = true
	}
	s.lock.Unlock()

	done := make(chan bool)

	go func() {
		defer func() {
			logDebug("closing connection for user: %s", conn.nick)
			s.removeConnection(conn)
			done <- true
		}()

		for {
			command, data, err := conn.Receive()
			conn.lastActive = time.Now()
			logDebug("conn.Receive(). Command: %s - Data: %s - Error: %v", command, data, err)
			if err != nil {
				notifyError(conn, err, command, data)
				return
			}

			if conn.debug {
				logDebug("debug enabled for connection. re-sending command back")

				debugData := map[string]interface{}{
					"command": command,
					"data":    string(data),
				}
				conn.Send(goulette.EventConnectionCommandReceived, debugData)
			}

			err = s.handleCommand(conn, command, data)
			if err != nil {
				logDebug("error handling command %s: %v", command, err)
				notifyError(conn, err, command, data)
				return
			}

			if command == goulette.CommandConnectionClose {
				logDebug("closing connection for user %s", conn.nick)
				return
			}
		}
	}()

	return done
}

func (s *Server) removeConnection(conn *Connection) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	delete(s.conns, conn)
	if s.waitingConn == conn {
		s.waitingConn = nil
	}

	if conn.nick != "" {
		delete(s.nicks, conn.nick)
	}

	if conn.currentChat != nil {
		s.endChat(conn.currentChat)
	}

	return conn.Close()
}

func notifyError(conn *Connection, err error, command string, data []byte) error {
	errData := map[string]interface{}{
		"error":   err.Error(),
		"command": command,
		"data":    string(data),
	}

	return conn.Send(goulette.EventConnectionError, errData)
}

// CommandHandler handles commands received over a connection
type CommandHandler func(conn *Connection, data []byte) error

// RegisterCommandHandler registers a function to handle a given command
func (s *Server) RegisterCommandHandler(command string, handler CommandHandler) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.handlers[command] = append(s.handlers[command], handler)
}

func (s *Server) handleCommand(conn *Connection, command string, data []byte) error {
	logDebug("handling command %s", command)
	s.lock.RLock()
	handlers, ok := s.handlers[command]
	s.lock.RUnlock()

	if !ok || len(handlers) == 0 {
		logDebug("No handlers registered for command %s", command)
		return nil
	}

	var res error

	for _, handler := range handlers {
		if err := handler(conn, data); err != nil {
			res = multierror.Append(res, err)
		}
	}

	return res
}

func (s *Server) close() {
	for conn := range s.conns {
		go s.removeConnection(conn)
	}
}

// RunLocking runs the given function
func (s *Server) RunLocking(f func(s *Server) error) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	return f(s)
}

type dataChatStarted struct {
	Participants []string `json:"participants"`
}

func (s *Server) newChat(conns ...*Connection) error {
	chat := newChat(conns...)
	s.chats[chat] = true
	for _, conn := range conns {
		conn.currentChat = chat

		data := dataChatStarted{
			Participants: chat.otherUsers(conn.nick),
		}
		conn.Send(goulette.EventChatStarted, data)
	}

	return nil
}

func (s *Server) endChat(chat *chat) error {
	if chat == nil {
		return nil
	}

	for _, conn := range chat.conns {
		conn.currentChat = nil
		conn.Send(goulette.EventChatEnded, nil)
	}

	delete(s.chats, chat)

	return nil
}

func (s *Server) findIdleBot(recentNicks nickList) *Connection {
	logDebug("finding idle bot...")

	for _, skipRecent := range []bool{true, false} {
		for bot := range s.botConns {
			logDebug("checking with bot %s", bot.nick)
			if bot.nick != "" && bot.currentChat == nil && (!skipRecent || !recentNicks.contains(bot.nick)) {
				logDebug("found idle bot: %s", bot.nick)
				return bot
			}
		}
	}

	return nil
}

func (s *Server) findWaitingConnection(recentNicks nickList) *Connection {
	logDebug("finding waiting connection...")

	// TODO: use a queue?
	conn := s.waitingConn

	if conn != nil && conn.nick != "" && !recentNicks.contains(conn.nick) {
		s.waitingConn = nil
		logDebug("found waiting connection: %s", conn.nick)
	}

	return conn
}

func (s *Server) findConnectionForNewChat(recentNicks nickList) *Connection {
	logDebug("findConnectionForNewChat: recent nicks: %v", recentNicks)
	conn := s.findWaitingConnection(recentNicks)
	if conn != nil {
		return conn
	}
	return s.findIdleBot(recentNicks)
}

func (s *Server) nextChat(conn *Connection) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	otherConn := s.findConnectionForNewChat(conn.recentNicks)
	if otherConn == nil {
		s.waitingConn = conn
		return errors.New("No connections found for new chat")
	}

	if otherConn.isBot {
		s.waitingConn = conn
	}

	if err := s.endChat(conn.currentChat); err != nil {
		return fmt.Errorf("next chat: ending initiator's chat: %v", err)
	}

	if err := s.endChat(otherConn.currentChat); err != nil {
		return fmt.Errorf("next chat: ending invited's chat: %v", err)
	}

	conn.recentNicks.add(otherConn.nick)
	otherConn.recentNicks.add(conn.nick)

	return s.newChat(conn, otherConn)
}

func (s *Server) isNickUsed(nick string) bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	_, used := s.nicks[nick]

	return used
}

func logDebug(format string, data ...interface{}) {
	now := time.Now().Format(time.RFC3339)
	fmt.Printf(fmt.Sprintf("%v - %s\n", now, format), data...)
}

type nickList struct {
	lock     sync.RWMutex
	nicks    []string
	maxItems uint8
}

func (nl *nickList) add(nick string) {
	nl.lock.Lock()
	nl.nicks = append(nl.nicks, nick)
	if len(nl.nicks) > int(nl.maxItems) {
		nl.nicks = nl.nicks[:nl.maxItems]
	}
	nl.lock.Unlock()
}

func (nl *nickList) contains(nick string) bool {
	nl.lock.RLock()

	for _, n := range nl.nicks {
		if n == nick {
			return true
		}
	}

	return false
}
