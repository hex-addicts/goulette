package server

import (
	"sync"
	"time"

	"gitlab.com/hex-addicts/goulette"

	multierror "github.com/hashicorp/go-multierror"
)

type chat struct {
	sync.RWMutex
	startedAt time.Time
	withBot   bool
	conns     map[string]*Connection
}

func newChat(conns ...*Connection) *chat {
	chat := &chat{
		conns:     make(map[string]*Connection),
		startedAt: time.Now(),
	}
	for _, conn := range conns {
		chat.conns[conn.nick] = conn
	}

	return chat
}

type speakData struct {
	Message string `json:"message"`
}

func (c *chat) speak(speaker, message string) error {
	c.RLock()
	defer c.RUnlock()

	logDebug("%s says %s", speaker, message)
	var res error
	for nick, conn := range c.conns {
		if nick != speaker {
			logDebug("sending message to %s", nick)
			err := conn.Send(goulette.EventChatMessageReceived, speakData{message})
			if err != nil {
				res = multierror.Append(res, err)
			}
		}
	}
	return res
}

func (c *chat) otherUsers(user string) []string {
	c.RLock()
	defer c.RUnlock()

	var nicks []string
	for nick := range c.conns {
		if nick != user {
			nicks = append(nicks, nick)
		}
	}

	return nicks
}
