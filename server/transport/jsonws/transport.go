package jsonws

import (
	"encoding/json"

	"golang.org/x/net/websocket"
)

type eventOut struct {
	Event string      `json:"event"`
	Data  interface{} `json:"data"`
}

type commandIn struct {
	Command string          `json:"command"`
	Data    json.RawMessage `json:"data"`
}

func NewTransport(conn *websocket.Conn) *Transport {
	return &Transport{
		conn: conn,
	}
}

type Transport struct {
	conn *websocket.Conn
}

func (t *Transport) Send(event string, data interface{}) error {
	evtOut := eventOut{
		Event: event,
		Data:  data,
	}
	return websocket.JSON.Send(t.conn, evtOut)
}

func (t *Transport) Receive() (command string, data []byte, err error) {
	cmdIn := commandIn{}
	err = websocket.JSON.Receive(t.conn, &cmdIn)

	return cmdIn.Command, cmdIn.Data, err
}

func (t *Transport) DecodeInto(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (t *Transport) Close() error {
	return t.conn.Close()
}
