package server

import (
	"errors"
	"fmt"

	"gitlab.com/hex-addicts/goulette"
)

// WithDefaultHandlers registers the default command handlers to the server
func WithDefaultHandlers(s *Server) {
	s.RegisterCommandHandler(goulette.CommandChatSpeak, handleChatSpeak)
	s.RegisterCommandHandler(goulette.CommandConnectionDebugEnable, handleConnectionDebugOn)
	s.RegisterCommandHandler(goulette.CommandConnectionDebugDisable, handleConnectionDebugOff)
	s.RegisterCommandHandler(goulette.CommandConnectionLogin, handleConnectionLogin)
	s.RegisterCommandHandler(goulette.CommandChatNext, handleChatNext)
}

type loginData struct {
	Nick string `json:"nick"`
}

func handleConnectionLogin(conn *Connection, data []byte) error {
	v := loginData{}
	err := conn.DecodeInto(data, &v)
	if err != nil {
		return fmt.Errorf("decoding login data: %v", err)
	}

	if v.Nick == "" {
		return errors.New("invalid nick for login (empty)")
	}

	if conn.server.isNickUsed(v.Nick) {
		return fmt.Errorf("nick is already being used: (%s)", v.Nick)
	}

	conn.nick = v.Nick
	logDebug("user logged-in with nick %s", v.Nick)
	if conn.isBot {
		return nil
	}

	return conn.server.nextChat(conn)
}

func handleConnectionDebugOn(conn *Connection, data []byte) error {
	conn.debug = true
	return nil
}

func handleConnectionDebugOff(conn *Connection, data []byte) error {
	conn.debug = false
	return nil
}

func handleChatSpeak(conn *Connection, data []byte) error {
	if conn.currentChat == nil {
		if conn.isBot {
			// Bots sometimes like to speak alone...
			return nil
		}
		return errors.New("chat.speak: theres no chat active for the connection")
	}

	if conn.nick == "" {
		return errors.New("chat.speak: the user has not logged in")
	}

	v := speakData{}
	err := conn.DecodeInto(data, &v)
	if err != nil {
		return err
	}

	conn.currentChat.speak(conn.nick, v.Message)

	return nil
}

func handleChatNext(conn *Connection, data []byte) error {
	if conn.nick == "" {
		return errors.New("chat.next: the user has not logged in")
	}

	return conn.server.nextChat(conn)
}
