package goulette

const (
	CommandChatNext               = "chat.cycle"
	CommandChatSpeak              = "chat.send_message"
	CommandConnectionClose        = "connection.close"
	CommandConnectionLogin        = "connection.login"
	CommandConnectionLogout       = "connection.logout"
	CommandConnectionDebugEnable  = "connection.debug.enable"
	CommandConnectionDebugDisable = "connection.debug.disable"
)
