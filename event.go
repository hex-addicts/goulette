package goulette

const (
	EventConnectionCommandReceived = "connection.command_received"
	EventConnectionError           = "connection.error_occurred"
	EventChatStarted               = "chat.started"
	EventChatEnded                 = "chat.ended"
	EventChatMessageReceived       = "chat.message_received"
)
