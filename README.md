# Goulette

Commands:

 command: chat.login
 data:
    - username string

 command: chat.next
 data:
    - withBot bool

command: chat.say
data:
    - message []byte

command: chat.receive
data:
    - user    string
    - message []byte